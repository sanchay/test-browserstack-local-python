FROM python:3.9-alpine
RUN apk update && apk add python3-dev \
                          gcc \
                          libc-dev \
                          libffi-dev
RUN pip install browserstack-local
COPY . .
CMD [ "python", "./main.py" ]