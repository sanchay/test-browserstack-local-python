# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.

from browserstack.local import Local


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.


# Press the green button in the gutter to run the script

# See PyCharm help at https://www.jetbrains.com/help/pycharm/


def run_test():
    # creates an instance of Local
    bs_local = Local()

    bs_local_args = {"key": "", "forcelocal": "true"}

    # starts the Local instance with the required arguments
    bs_local.start(**bs_local_args)

    # check if BrowserStack local instance is running
    print(bs_local.isRunning())

    # stop the Local instance
    bs_local.stop()


if __name__ == '__main__':
    print_hi('PyCharm')
    run_test()
